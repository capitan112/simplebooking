//
//  CARPCConnection.h
//  CollectionViewControllertest
//
//  Created by Капитан on 01.09.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CARPCConnection : NSObject

@property (nonatomic, assign) BOOL isConnected;
- (NSDictionary *)createRequestWithDict:(NSDictionary *)dict;
    
@end
