//
//  main.m
//  CollectionViewControllertest
//
//  Created by Капитан on 30.08.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CAAppDelegate class]));
    }
}
