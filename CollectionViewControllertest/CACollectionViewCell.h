#import <UIKit/UIKit.h>

@interface CACollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *cellLabel;
@property (nonatomic, assign) BOOL enable;

@end
