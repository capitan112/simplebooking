//
//  CARPCConnection.m
//  CollectionViewControllertest
//
//  Created by Капитан on 01.09.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "CARPCConnection.h"

static const NSString *companyLogin    = @"develop";
static const NSString *urlToken        = @"http://user-api.ru.simplybook.me/login";
static const NSString *urlRegistration = @"https://user-api.simplybook.me/";
static const NSString *keyAPI         = @"7f70f455304274c550704446e0c8d772655b14236f5d3c360168bcfbb06ce221";

@interface CARPCConnection ()

@property (nonatomic, copy) NSString *token;
@property (nonatomic, strong) NSMutableData *receivedData;
@property (nonatomic, strong) NSUserDefaults *defaults;

@end

@implementation CARPCConnection

- (id)init {
    self = [super init];
    if (self) {
        _defaults = [NSUserDefaults standardUserDefaults];
        _receivedData = [[NSMutableData alloc] init];
        [self getToken];
//        _token = [_defaults objectForKey:@"token"];
//        if (_token) {
//            [self getToken];
//        }
    }
    return self;
}

#pragma mark - requests

- (void)getToken {
    NSInteger idRequest = arc4random();
    NSDictionary *rpcDict = @{
                              @"jsonrpc" : @"2.0",
                              @"id" : @(idRequest),
                              @"method" : @"getToken",
                              @"params" : @[companyLogin, keyAPI]
                              };
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:rpcDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSDictionary *defaultHeader = @{
                                    @"Accept" : @"application/json",
                                    @"Content-Type": @"application/json"
                                    };
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[urlToken copy]]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:2];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:defaultHeader];
    [request setHTTPBody:jsonData];
    NSDictionary *serverDict = [self getDictFromServer:request];
    _token = serverDict[@"result"];
    if (serverDict) {
        [_defaults setObject:_token forKey:@"token"];
        [_defaults synchronize];
        _isConnected = YES;
        NSLog(@"token setted");
    } else {
        _isConnected = NO;
        NSLog(@"token is nill");
    }
}

- (NSDictionary *)createRequestWithDict:(NSDictionary *)dict {
    NSInteger idRequest = arc4random();
    NSDictionary *dictWithParamets = @{
                                       @"jsonrpc" : @"2.0",
                                       @"id" : @(idRequest),
                                       };
    NSMutableDictionary *rpcDict = [NSMutableDictionary dictionaryWithDictionary:dictWithParamets];
    [rpcDict addEntriesFromDictionary:dict];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:rpcDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSDictionary *defaultHeader = @{
                                    @"Accept" : @"application/json",
                                    @"Content-Type": @"application/json",
                                    @"X-Company-Login" : companyLogin,
                                    @"X-Token" : _token
                                    };
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                    initWithURL:[NSURL URLWithString:[urlRegistration copy]]
                                    cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                    timeoutInterval:2];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:defaultHeader];
    [request setHTTPBody:jsonData];
    
    return [self getDictFromServer:request];
}


#pragma mark - getData from Server

- (NSDictionary *)getDictFromServer:(NSURLRequest *)request {
    NSURLResponse *response = nil;
    NSData *serverData = [NSURLConnection sendSynchronousRequest:request
                                               returningResponse:&response
                                                           error:nil];
    if (serverData == nil) {
        NSLog(@"Error getinng data form server");
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"No internet connection"
                                                         message:@"Please check connection"
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles: nil];
        [alert show];
        return nil;
    }
    
    NSError *dictionaryParceError;
    NSDictionary *serverDictionary = [NSJSONSerialization JSONObjectWithData:serverData
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:&dictionaryParceError];
    return serverDictionary;
}

@end
