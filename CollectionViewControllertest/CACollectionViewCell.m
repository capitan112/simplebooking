#import "CACollectionViewCell.h"

@implementation CACollectionViewCell

- (void)prepareForReuse {
    [super prepareForReuse];
    self.backgroundColor = [UIColor colorWithRed:196/255.0f green:255/255.0f blue:137/255.0f alpha:1.0f];
}

- (void)setSelected:(BOOL)selected {
    if (self.enable) {
        UIColor *defaultColor = [UIColor colorWithRed:196/255.0f green:255/255.0f blue:137/255.0f alpha:1.0f];
        //цвет выбора ячейки
        UIColor *selectedColor = [UIColor colorWithRed:1255/255.0f green:255/255.0f blue:137/255.0f alpha:1.0f];
        self.backgroundColor = selected ? selectedColor : defaultColor;
    }
    [super setSelected:selected];
}


@end
