//
//  CAViewController.m
//  CollectionViewControllertest
//
//  Created by Капитан on 30.08.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "CAViewController.h"
#import "CACollectionViewCell.h"
#import "CACollectionHeaderView.h"
#import "CARPCConnection.h"

static NSString *HeaderIdentifier = @"HeaderIdentifier";
static const NSString *requiredDay = @"2014-09-04";

@interface CAViewController (){
    int numberOfItemsInSection;
    CACollectionViewCell *cell;
    CARPCConnection *rpcConnection;
    NSDictionary *shedduleTimeDict;
    NSArray *sortedKeyHoursArray;
    int selectedPath;
}

@end

@implementation CAViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    numberOfItemsInSection = 4;
    rpcConnection = [[CARPCConnection alloc] init];
//    if (rpcConnection.isConnected) {
//        [self getDataFromServer];
//    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    if (rpcConnection.isConnected) {
         [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [self getDataFromServer];
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }

}

- (void)getDataFromServer {
    NSDictionary *getStartTimeMatrix = @{
                                         @"method" : @"getStartTimeMatrix",
                                         @"params" : @[requiredDay, requiredDay, @(1), @(1) ]
                                         };
    NSDictionary *startTimeTemp = [rpcConnection createRequestWithDict:getStartTimeMatrix];
    NSArray *startTimeData = [[NSArray alloc]initWithArray:startTimeTemp[@"result"][requiredDay]];
    NSMutableDictionary *shedduleTime = [[NSMutableDictionary alloc] init];
    for (NSString *time in startTimeData) {
        NSString *truncatedString = [time substringToIndex:[time length] - 3];
        [shedduleTime setValue:@"available" forKey:truncatedString];
    }
    NSDictionary *getReservedTime = @{
                                      @"method" : @"getReservedTime",
                                      @"params" : @[requiredDay, requiredDay, @(1), @(1)]
                                      };
    NSDictionary *reservedTimeTemp = [rpcConnection createRequestWithDict:getReservedTime];
    NSArray *reservedTimeData = [[NSArray alloc]initWithArray:reservedTimeTemp[@"result"][requiredDay]];
    for (NSDictionary *busyTime in reservedTimeData) {
        for(NSDictionary *busyHours in busyTime[@"events"]) {
            [shedduleTime setValue:@"busy" forKey:busyHours[@"from"]];
        }
    }
    NSArray* keys = [shedduleTime allKeys];
    sortedKeyHoursArray = [keys sortedArrayUsingComparator:^(id a, id b) {
        return [a compare:b options:NSNumericSearch];
    }];
    shedduleTimeDict = [shedduleTime copy];
//    NSLog(@"shedduleTime %@", shedduleTime);
}

#pragma mark - delegate method

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [shedduleTimeDict count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 4;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    cell = (CACollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell Identifier" forIndexPath:indexPath];
//    shedduleTimeDict
    NSString *hoursKey = sortedKeyHoursArray[indexPath.row];
    NSString *hoursAviability = shedduleTimeDict[hoursKey];
    if ([hoursAviability isEqualToString:@"available"]) {
        cell.enable = YES;
    } else {
        cell.enable = NO;
    }
    cell.layer.masksToBounds = NO;
    cell.layer.cornerRadius = 8; // rounded corners
    cell.layer.shadowOffset = CGSizeMake(-15, 20);
    cell.layer.shadowRadius = 5;
    cell.layer.shadowOpacity = 0.8;
    cell.layer.shadowOffset = CGSizeMake(6, 6);
    cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:cell.bounds].CGPath;
    //тень
    CGColorRef cellColor = [UIColor colorWithRed:9/255.0f green:9/255.0f blue:9/255.0f alpha:1.0f].CGColor;
    cell.layer.shadowColor = cellColor;
    if (!cell.enable) {
        //цвет busy
        cell.backgroundColor = [UIColor colorWithRed:246/255.0f green:145/255.0f blue:160/255.0f alpha:1.0f];
        cell.cellLabel.text = @"busy";
    } else {
        //цвет, для выбора
        cell.backgroundColor = [UIColor colorWithRed:196/255.0f green:255/255.0f blue:137/255.0f alpha:1.0f];
      cell.cellLabel.text = hoursKey;
    }
//  NSLog(@"cell enable %d", cell.enable);
    return cell;
}

#pragma mark - Cell

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *hoursKey = sortedKeyHoursArray[indexPath.row];
    NSString *hoursAviability = shedduleTimeDict[hoursKey];
    
    if ([hoursAviability isEqualToString:@"available"]) {
        selectedPath = indexPath.row;
    } else {
        selectedPath = - 1;
    }
}

#pragma mark Header

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    CACollectionHeaderView *headerView = (CACollectionHeaderView *)[collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:HeaderIdentifier forIndexPath:indexPath];
       [headerView setText:[NSString stringWithFormat:@"Master %ld", (long)indexPath.section + 1]];
        headerView.additionaFields.delegate = self;
        headerView.additionaFields.enabled = YES;
//    if (indexPath.section == 0) {
//        [headerView setText:@"Tap on button to reverse time"];
//    } else {
//        [headerView setText:[NSString stringWithFormat:@"Master %ld", (long)indexPath.section]];
//    }
    return headerView;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"Text field did begin editing");
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSLog(@"Text field ended editing");
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - dynamic cell size

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    int spaceBetweenCell = (300/numberOfItemsInSection) - 54;
    int minSpaceBetweenCell = 6;
    
    return spaceBetweenCell > 0 ? spaceBetweenCell : minSpaceBetweenCell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - booking

- (IBAction)booked:(id)sender {
    NSLog(@"booking");
//    if (selectedPath < 0 || !selectedPath) {
//        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Unspecified time"
//                                                         message:@"Please choose time for booking"
//                                                        delegate:self
//                                               cancelButtonTitle:@"Ok"
//                                               otherButtonTitles: nil];
//        [alert show];
//        return;
//    }
//    NSString *requiredTime = sortedKeyHoursArray[selectedPath];
//    NSDictionary *book  = @{
//                            @"method" : @"book",
//                            @"params" : @[@(1), @(1), requiredDay, requiredTime, @{@"name": @"Dad", @"email": @"plrs@mail.ru", @"phone": @"+380504191155"}, @"true" ]
//                            };
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//    NSDictionary *bookResult =  [rpcConnection createRequestWithDict:book];
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
//    if (bookResult[@"error"]) {
//        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Current time has choosed"
//                                                         message:@"Please choose time for booking"
//                                                        delegate:self
//                                               cancelButtonTitle:@"Ok"
//                                               otherButtonTitles: nil];
//        [alert show];
//        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//        [self getDataFromServer];
//        [self.collectionView reloadData];
//        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
//        
//        return;
//    }
//    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Booked"
//                                                     message:[NSString stringWithFormat: @"You have booked %@",requiredTime]
//                                                    delegate:self
//                                           cancelButtonTitle:@"Ok"
//                                           otherButtonTitles: nil];
//    [alert show];
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//    [self getDataFromServer];
//    [self.collectionView reloadData];
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
}


@end
