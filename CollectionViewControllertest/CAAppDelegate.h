//
//  ACAppDelegate.h
//  CollectionViewControllertest
//
//  Created by Капитан on 30.08.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
